FROM --platform=linux/arm64/v8 adoptopenjdk:8u292-b10-jre-hotspot-focal

COPY target/*.jar app.jar

ENV PROFILE loc

ENTRYPOINT ["java","-jar", "-Dspring.profiles.active=${PROFILE}", "/app.jar"]